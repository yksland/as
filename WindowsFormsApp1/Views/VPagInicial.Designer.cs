﻿namespace WindowsFormsApp1.Views
{
    partial class VPagInicial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonDepositar = new System.Windows.Forms.Button();
            this.buttonAlterar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonCarteira = new System.Windows.Forms.Button();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.fecharContratoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listBoxVigilancia = new System.Windows.Forms.ListBox();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonDepositar
            // 
            this.buttonDepositar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDepositar.Location = new System.Drawing.Point(31, 370);
            this.buttonDepositar.Name = "buttonDepositar";
            this.buttonDepositar.Size = new System.Drawing.Size(110, 39);
            this.buttonDepositar.TabIndex = 0;
            this.buttonDepositar.Text = "Depositar";
            this.buttonDepositar.UseVisualStyleBackColor = true;
            this.buttonDepositar.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonAlterar
            // 
            this.buttonAlterar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAlterar.Location = new System.Drawing.Point(352, 370);
            this.buttonAlterar.Name = "buttonAlterar";
            this.buttonAlterar.Size = new System.Drawing.Size(116, 39);
            this.buttonAlterar.TabIndex = 1;
            this.buttonAlterar.Text = "Alterar Perfil";
            this.buttonAlterar.UseVisualStyleBackColor = true;
            this.buttonAlterar.Click += new System.EventHandler(this.buttonAlterar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(27, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "Lista de Vigilância";
            // 
            // buttonCarteira
            // 
            this.buttonCarteira.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCarteira.Location = new System.Drawing.Point(665, 370);
            this.buttonCarteira.Name = "buttonCarteira";
            this.buttonCarteira.Size = new System.Drawing.Size(107, 39);
            this.buttonCarteira.TabIndex = 3;
            this.buttonCarteira.Text = "Carteira";
            this.buttonCarteira.UseVisualStyleBackColor = true;
            this.buttonCarteira.Click += new System.EventHandler(this.buttonCarteira_Click);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fecharContratoToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(150, 26);
            // 
            // fecharContratoToolStripMenuItem
            // 
            this.fecharContratoToolStripMenuItem.Name = "fecharContratoToolStripMenuItem";
            this.fecharContratoToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.fecharContratoToolStripMenuItem.Text = "Criar Contrato";
            this.fecharContratoToolStripMenuItem.Click += new System.EventHandler(this.fecharContratoToolStripMenuItem_Click);
            // 
            // listBoxVigilancia
            // 
            this.listBoxVigilancia.AllowDrop = true;
            this.listBoxVigilancia.ColumnWidth = 15;
            this.listBoxVigilancia.ContextMenuStrip = this.contextMenuStrip;
            this.listBoxVigilancia.Cursor = System.Windows.Forms.Cursors.Default;
            this.listBoxVigilancia.FormattingEnabled = true;
            this.listBoxVigilancia.Location = new System.Drawing.Point(31, 36);
            this.listBoxVigilancia.MultiColumn = true;
            this.listBoxVigilancia.Name = "listBoxVigilancia";
            this.listBoxVigilancia.Size = new System.Drawing.Size(741, 212);
            this.listBoxVigilancia.TabIndex = 4;
            // 
            // VPagInicial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.listBoxVigilancia);
            this.Controls.Add(this.buttonCarteira);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonAlterar);
            this.Controls.Add(this.buttonDepositar);
            this.Name = "VPagInicial";
            this.Text = "VPagInicial";
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonDepositar;
        private System.Windows.Forms.Button buttonAlterar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonCarteira;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fecharContratoToolStripMenuItem;
        private System.Windows.Forms.ListBox listBoxVigilancia;
    }
}