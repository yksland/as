﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Views
{
    public partial class VRegisto : Form
    {
        public VRegisto()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string resposta = Program.cRegisto.Registar(textBoxNome.Text, textBoxPais.Text, textBoxEmail.Text, textBoxUsername.Text, textBoxPass.Text);
            MessageBox.Show(resposta);

            if(resposta == "Utilizador adicionado com sucesso")
            {
                this.Hide();
                VLogin novo = new VLogin();
                novo.ShowDialog();
                this.Close();
            }


        }
        
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void VRegisto_Load(object sender, EventArgs e)
        {
            
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
            VInicio novo = new VInicio();
            novo.ShowDialog();
            this.Close();
        }
    }
}
