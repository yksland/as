﻿namespace WindowsFormsApp1.Views
{
    partial class VContrato
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.buttonEfetuar = new System.Windows.Forms.Button();
            this.buttonCancelar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelPreco = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.txbQuantidade = new System.Windows.Forms.TextBox();
            this.txbTP = new System.Windows.Forms.TextBox();
            this.txbSL = new System.Windows.Forms.TextBox();
            this.radioButtonVender = new System.Windows.Forms.RadioButton();
            this.radioButtonComprar = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Contrato";
            // 
            // buttonEfetuar
            // 
            this.buttonEfetuar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEfetuar.Location = new System.Drawing.Point(280, 271);
            this.buttonEfetuar.Name = "buttonEfetuar";
            this.buttonEfetuar.Size = new System.Drawing.Size(100, 38);
            this.buttonEfetuar.TabIndex = 3;
            this.buttonEfetuar.Text = "Efetuar";
            this.buttonEfetuar.UseVisualStyleBackColor = true;
            this.buttonEfetuar.Click += new System.EventHandler(this.buttonEfetuar_Click);
            // 
            // buttonCancelar
            // 
            this.buttonCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancelar.Location = new System.Drawing.Point(419, 271);
            this.buttonCancelar.Name = "buttonCancelar";
            this.buttonCancelar.Size = new System.Drawing.Size(92, 38);
            this.buttonCancelar.TabIndex = 4;
            this.buttonCancelar.Text = "Cancelar";
            this.buttonCancelar.UseVisualStyleBackColor = true;
            this.buttonCancelar.Click += new System.EventHandler(this.buttonCancelar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(302, 187);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "TakeProfit:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(302, 221);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "StopLoss:";
            // 
            // labelPreco
            // 
            this.labelPreco.AutoSize = true;
            this.labelPreco.Location = new System.Drawing.Point(302, 124);
            this.labelPreco.Name = "labelPreco";
            this.labelPreco.Size = new System.Drawing.Size(38, 13);
            this.labelPreco.TabIndex = 7;
            this.labelPreco.Text = "Preço:";
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(302, 154);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(65, 13);
            this.label.TabIndex = 8;
            this.label.Text = "Quantidade:";
            // 
            // txbQuantidade
            // 
            this.txbQuantidade.Location = new System.Drawing.Point(373, 151);
            this.txbQuantidade.Name = "txbQuantidade";
            this.txbQuantidade.Size = new System.Drawing.Size(100, 20);
            this.txbQuantidade.TabIndex = 9;
            // 
            // txbTP
            // 
            this.txbTP.Location = new System.Drawing.Point(373, 184);
            this.txbTP.Name = "txbTP";
            this.txbTP.Size = new System.Drawing.Size(100, 20);
            this.txbTP.TabIndex = 10;
            // 
            // txbSL
            // 
            this.txbSL.Location = new System.Drawing.Point(373, 214);
            this.txbSL.Name = "txbSL";
            this.txbSL.Size = new System.Drawing.Size(100, 20);
            this.txbSL.TabIndex = 11;
            // 
            // radioButtonVender
            // 
            this.radioButtonVender.AutoSize = true;
            this.radioButtonVender.Location = new System.Drawing.Point(297, 49);
            this.radioButtonVender.Name = "radioButtonVender";
            this.radioButtonVender.Size = new System.Drawing.Size(59, 17);
            this.radioButtonVender.TabIndex = 12;
            this.radioButtonVender.TabStop = true;
            this.radioButtonVender.Text = "Vender";
            this.radioButtonVender.UseVisualStyleBackColor = true;
            // 
            // radioButtonComprar
            // 
            this.radioButtonComprar.AutoSize = true;
            this.radioButtonComprar.Location = new System.Drawing.Point(419, 49);
            this.radioButtonComprar.Name = "radioButtonComprar";
            this.radioButtonComprar.Size = new System.Drawing.Size(64, 17);
            this.radioButtonComprar.TabIndex = 13;
            this.radioButtonComprar.TabStop = true;
            this.radioButtonComprar.Text = "Comprar";
            this.radioButtonComprar.UseVisualStyleBackColor = true;
            // 
            // VContrato
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.radioButtonComprar);
            this.Controls.Add(this.radioButtonVender);
            this.Controls.Add(this.txbSL);
            this.Controls.Add(this.txbTP);
            this.Controls.Add(this.txbQuantidade);
            this.Controls.Add(this.label);
            this.Controls.Add(this.labelPreco);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonCancelar);
            this.Controls.Add(this.buttonEfetuar);
            this.Controls.Add(this.label1);
            this.Name = "VContrato";
            this.Text = "VContrato";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonEfetuar;
        private System.Windows.Forms.Button buttonCancelar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelPreco;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.TextBox txbQuantidade;
        private System.Windows.Forms.TextBox txbTP;
        private System.Windows.Forms.TextBox txbSL;
        private System.Windows.Forms.RadioButton radioButtonVender;
        private System.Windows.Forms.RadioButton radioButtonComprar;
    }
}