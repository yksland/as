﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Views
{
    public partial class VLogin : Form
    {
        public VLogin()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void buttonEntrar_Click(object sender, EventArgs e)
        {

            string respostaLogin = Program.cLogin.Login(textBoxEmail.Text, textBoxPass.Text); //da erro aqui, da uma vista de olhos!!!!
            MessageBox.Show(respostaLogin);
            
            if(respostaLogin == "Login efetuado com sucesso!")
            {
                this.Hide();
                VPagInicial novo = new VPagInicial();
                novo.ShowDialog();
                this.Close();
            }

        }

        private void buttonRegistar_Click(object sender, EventArgs e)
        {
            this.Hide();
            VRegisto novo = new VRegisto();
            novo.ShowDialog();
            this.Close();
        }

        private void VLogin_Load(object sender, EventArgs e)
        {

        }
    }
}
