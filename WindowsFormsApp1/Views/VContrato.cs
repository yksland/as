﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Views
{
    public partial class VContrato : Form
    {
        public VContrato()
        {
            InitializeComponent();
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
            VPagInicial novo = new VPagInicial();
            novo.ShowDialog();
            this.Close();
        }

        private void buttonEfetuar_Click(object sender, EventArgs e)
        {
            string resposta = Program.cContrato.CriarCFD();
            MessageBox.Show(resposta);

            this.Hide();
            VPagInicial novo = new VPagInicial();
            novo.ShowDialog();
            this.Close();
        }
    }
}
