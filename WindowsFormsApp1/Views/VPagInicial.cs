﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Views
{
    public partial class VPagInicial : Form
    {
        public VPagInicial()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            VDepositar novo = new VDepositar();
            novo.ShowDialog();
            this.Close();
        }

        private void buttonAlterar_Click(object sender, EventArgs e)
        {
            this.Hide();
            VEditar novo = new VEditar();
            novo.ShowDialog();
            this.Close();
        }

        private void buttonCarteira_Click(object sender, EventArgs e)
        {
            this.Hide();
            VCarteira novo = new VCarteira();
            novo.ShowDialog();
            this.Close();
        }

        private void fecharContratoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            VContrato novo = new VContrato();
            novo.ShowDialog();
            this.Close();
        }


    }
}
