﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Views
{
    public partial class VInicio : Form
    {
        public VInicio()
        {
            InitializeComponent();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            this.Hide();
            VLogin novo = new VLogin();
            novo.ShowDialog();
            this.Close();
        }

        private void buttonRegistar_Click(object sender, EventArgs e)
        {
            this.Hide();
            VRegisto novo = new VRegisto();
            novo.ShowDialog();
            this.Close();

           
        }   
    }
}
