﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Models
{
    public class Carteira
    {
       public int numContratos { get; set; }
        float saldoInicial { get; set; }
        float entradaDinheiro { get; set; }
        float saidaDinheiro { get; set; }
        float lp { get; set; }
        float valorFinal { get; set; }
        float saldoAtual { get; set; }
        List<Contrato> ListaContratos { get; set; }

        int idContrato { get; set; }
    }
}
