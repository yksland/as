﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WindowsFormsApp1.Models
{
    public class Utilizador
    {
        public int idUtilizador { get; set; }
        public string nome { get; set; }
        public DateTime dataNascimento { get; set; }
        public string pais { get; set; }
        public string email { get; set; }
        public  string Username { get; set; }

        public string password { get; set; }

        public Carteira carteira { get; set; }
     
    }
}
