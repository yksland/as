﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Models
{
   public class Broker
    {
        int idBroker { get; set; }
        string nome { get; set; }
        List<Acao> a { get; set; }
        List<Commodity> c { get; set; }
        
    }
}
