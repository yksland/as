﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Models
{
   public class Contrato
    {
        int idContrato { get; set; }
        string tipo { get; set; }
        Boolean estado { get; set; }
        float valorComercial { get; set; }
        float takeProfit { get; set; }
        float stopLoss { get; set; }
        float medida { get; set; }

        

        int idUtilizador { get; set; }
    }
}
