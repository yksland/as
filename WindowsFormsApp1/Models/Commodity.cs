﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Models
{
   public class Commodity
    {
        int idAtivo { get; set; }
        string nome { get; set; }
        float valor { get; set; }
        float percentagem { get; set; }
        int quantidade { get; set; }
    }
}
