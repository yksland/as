﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Common;
using System.Configuration;
using WindowsFormsApp1.Views;
using WindowsFormsApp1.Controllers;
using WindowsFormsApp1.Models;

namespace WindowsFormsApp1
{
    static class Program
    {
        public static Utilizador utilizador { get; private set; }
        public static VInicio Vinicio { get; private set; }
        public static VRegisto Vregisto { get; private set; }
        public static VLogin Vlogin { get; private set; }
        public static VEditar Veditar { get; private set; }

        public static VCarteira Vcarteira { get; private set; }

        public static VContrato Vcontrato { get; private set; }
        public static VDepositar Vdepositar { get; private set; }

        public static VPagInicial Vpaginicial { get; private set; }

        public static ControllerRegisto cRegisto { get; private set; }
        public static ControllerLogin cLogin { get; private set; }
        public static ControllerContrato cContrato { get; private set; }
        public static ControllerEditar cEditar { get; private set; }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            utilizador = new Utilizador();

            Vinicio = new VInicio();

            Vregisto = new VRegisto();
            Vlogin = new VLogin();
            Vcarteira = new VCarteira();
            Vcontrato = new VContrato();
            Veditar = new VEditar();
            Vdepositar = new VDepositar();
            Vpaginicial = new VPagInicial();

            cRegisto = new ControllerRegisto();
            cLogin = new ControllerLogin();
            cContrato = new ControllerContrato();

            

           /* string provider = ConfigurationManager.AppSettings["provider"];
            string connectionString = ConfigurationManager.AppSettings["connectionString"];

            DbProviderFactory factory = DbProviderFactories.GetFactory(provider);

            using (DbConnection connection = factory.CreateConnection())
            {
                if (connection == null)
                {
                    Console.WriteLine("Connection Error");
                    Console.ReadLine();
                    return;
                }
                connection.ConnectionString = connectionString;

                connection.Open();

                DbCommand command = factory.CreateCommand();

                if (command == null)
                {
                    Console.WriteLine("Command Error");
                    Console.ReadLine();
                    return;
                }

                command.Connection = connection;
                command.CommandText = "Select * from Acao";

                using (DbDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        Console.WriteLine("Test");
                    }
                }*/
                Application.EnableVisualStyles();
                Application.Run(Vinicio);
            }
        }
    }
//}
