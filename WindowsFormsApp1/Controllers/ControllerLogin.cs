﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.DB;

namespace WindowsFormsApp1.Controllers
{
    class ControllerLogin
    {
        public string Login(string email, string pass)
        {
            if(String.IsNullOrEmpty(email) || String.IsNullOrEmpty(pass))
            {
                return ("Preencher todos os campos.");

            }
            else
            {
                ASDataSet2 x = new ASDataSet2();
                DB.ASDataSet2TableAdapters.UtilizadorTableAdapter userAdapter = new DB.ASDataSet2TableAdapters.UtilizadorTableAdapter();

                if ((userAdapter.GetDataByLog(email, pass).Rows.Count ==1))
                {
                    return ("O email ou password estão errados.");
                }
             
                else
                {
                    return ("Login efetuado com sucesso!");
                }

            }
        }
    }
}
