﻿using System;
using System.Runtime.Serialization;

namespace WindowsFormsApp1.Controllers
{
    [Serializable]
    internal class Execao : Exception
    {
        public Execao()
        {
        }

        public Execao(string message) : base(message)
        {
        }

        public Execao(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected Execao(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}