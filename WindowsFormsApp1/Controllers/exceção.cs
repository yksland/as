﻿using System;
using System.Runtime.Serialization;

namespace WindowsFormsApp1.Controllers
{
    [Serializable]
    internal class exceção : Exception
    {
        public exceção()
        {
        }

        public exceção(string message) : base(message)
        {
        }

        public exceção(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected exceção(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}