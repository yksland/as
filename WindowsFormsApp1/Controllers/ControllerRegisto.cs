﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Models;
using WindowsFormsApp1.DB;

namespace WindowsFormsApp1.Controllers
{
    class ControllerRegisto
    {

        public string Registar(string nome, string pais, string email, string username, string password)
        {

            Utilizador utilizador = new Utilizador();
            if (String.IsNullOrEmpty(nome) || String.IsNullOrEmpty(pais) || String.IsNullOrEmpty(email) || String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password))
            {
                return ("Preencher todos os campos");
            }
            else
            {
                ASDataSet2 x = new ASDataSet2();
                DB.ASDataSet2TableAdapters.UtilizadorTableAdapter user = new DB.ASDataSet2TableAdapters.UtilizadorTableAdapter();

                if(user.GetDataByEmail(email).Rows.Count == 1 || user.GetDataByUName(username).Rows.Count ==1)
                {
                    return ("Utilizador já existente");
                }
                else
                {
                    user.Insert(nome, pais, email, username, password);
                    return ("Utilizador adicionado com sucesso");
                }
               

                
            }
                //string connectionString = @"Data Source=DESKTOP-4RFQK7D\BARBARA;Initial Catalog=ArquiteturaSw;Integrated Security=True";
                //SqlConnection sqlCon = new SqlConnection(connectionString);

                //string commandString = "insert into Utilizador Values(@N,@P,@E,@U,@PS)";// + textBoxNome.Text + "," + textBoxPais.Text + "," + textBoxEmail.Text + "," + textBoxUsername.Text + "," + textBoxPass.Text + ")";
                //SqlCommand sqlCmd = new SqlCommand(commandString, sqlCon);

                //sqlCmd.Parameters.AddWithValue("N", nome);
                //sqlCmd.Parameters.AddWithValue("P", pais);
                //sqlCmd.Parameters.AddWithValue("E", email);
                //sqlCmd.Parameters.AddWithValue("U", username);
                //sqlCmd.Parameters.AddWithValue("PS", password);

                //sqlCon.Open();

                //int i = sqlCmd.ExecuteNonQuery();
                //sqlCon.Close();

                //return ("Utilizador add a bd com sucesso");


            }
        }
}



      

